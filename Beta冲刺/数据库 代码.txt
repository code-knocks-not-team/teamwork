---4.3.1 创建数据库 
CREATE DATABASE TAGS;
---4.3.2 创建表 
use tags;
CREATE TABLE `user` 
( `id` int(11) NOT NULL AUTO_INCREMENT, 
`userName` varchar(24) CHARACTER SET utf8 NOT NULL UNIQUE, 
`password` varchar(18) NOT NULL, 
`sex` varchar(3) CHARACTER SET utf8 DEFAULT NULL, 
`tel` int(11) DEFAULT NULL, 
`messageNum` int(10) DEFAULT NULL, 
`sno` int(10) DEFAULT NULL,
`grade` int(20) DEFAULT NULL, 
`department` varchar(30) CHARACTER SET utf8 DEFAULT NULL, 
`status` varchar(20) CHARACTER SET utf8 DEFAULT NULL, 
PRIMARY KEY (`id`) ) ENGINE=MyISAM DEFAULT CHARSET=utf8; 

CREATE TABLE `manager` 
( `id` int(11) NOT NULL AUTO_INCREMENT, 
`username` varchar(24) CHARACTER SET utf8 NOT NULL, 
`password` varchar(18) NOT NULL, 
`sex` varchar(3) CHARACTER SET utf8 DEFAULT NULL, 
`tel` int(11) DEFAULT NULL, `messagenum` int(10) DEFAULT NULL, 
`sno` int(10) DEFAULT NULL, 
`grade` int(20) DEFAULT NULL, 
`department` varchar(30) CHARACTER SET utf8 DEFAULT NULL,
`status` varchar(20) CHARACTER SET utf8 NOT NULL, 
PRIMARY KEY (`id`) ) ENGINE=MyISAM DEFAULT CHARSET=utf8; 

CREATE TABLE `message` 
( `id` int(20) NOT NULL AUTO_INCREMENT, 
`author` varchar(30) DEFAULT NULL, 
`matter` longtext CHARACTER SET utf8 DEFAULT NULL,
`pic` varchar(100) DEFAULT '', 
`uptime` datetime DEFAULT NULL, 
`area` varchar(50) NOT NULL, 
`likes` int(11) DEFAULT NULL, 
`complaints` int(11) DEFAULT NULL, 
`status` varchar(20) DEFAULT NULL, 
`comments` int(11) DEFAULT NULL, 
PRIMARY KEY (`id`) ) ENGINE=MyISAM DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC; 

CREATE TABLE `comment` 
( `id` int(50) NOT NULL AUTO_INCREMENT, 
`messageid` int(50) NOT NULL, 
`metter` varchar(255) CHARACTER SET utf8 NOT NULL DEFAULT '', 
`pic` varchar(255) DEFAULT '', 
`like` int(10) NOT NULL, 
`complaints` int(10) NOT NULL, 
`status` varchar(10) DEFAULT '', 
PRIMARY KEY (`id`) ) ENGINE=MyISAM DEFAULT CHARSET=utf8;

---临时保存投稿表
CREATE TABLE `save` (
`id`  int(20) NOT NULL AUTO_INCREMENT ,
`author`  varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' ,
`matter`  longtext CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL ,
`pic`  binary(255) NULL ,
`uptime`  datetime NULL ,
`area`  varchar(50) NOT NULL ,
PRIMARY KEY (`id`)
)
;
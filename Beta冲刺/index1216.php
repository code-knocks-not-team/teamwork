<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8" />
		<title>首页</title>
		<link rel="stylesheet" type="text/css" href="./css/css.css"/>
	</head>
	<body>
		<!-- 导航栏 -->
		<div id="top">
			<div class="container">
				<div class="logo" style="float: left;margin-right:0px ">
					<img src="./img/winter_tree.png">
				</div>
				<div class="title">
					<ul>
						<m>针不戳</m>
					</ul>
				</div>
				<div class="index">
					<ul>
						<li class="top_sub" style="background-color: #04c3e0;"><a href="index.php" class="link"><n>主页</n></a></li>
						<li class="top_sub"><a href="UserCenter.html" class="link1"><n>个人中心</n></a></li>
						<li class="top_sub"><a href="UpMassage.html" class="link1"><n>发布投稿</n></a></li>
					</ul>
				</div>
				<div class="login">
					<ul>
						<li>
							<img src="./img/登录.png" style="width: 20px;">
							<a href="login.html"><n>登录</n></a>
						</li>
						<li>
							<n>/</n>
						</li>
						<li>
							<a href="register.html"><n>注册</n></a>
						</li>
					</ul>
				</div>
			</div>
		</div>
		<!-- 内容部分 -->
		<div id="massage">
		<!-- 分区部分 -->
		<div id="header">
			<div class="container">
				<ul>
					<li class="head_sub" style="background-color: #b6f2fa;">
						<img src="./img/首页.png" style="left: 27px;top: 11px;">
						<a href="index.php"><n>综合</n></a>
					</li>
					<li class="head_sub">
						<img src="./img/作品.png" style="left: 11px;top: 12px;">
						<a href="index_lost.php"><n>失物招领</n></a>
					</li>
					<li class="head_sub">
						<a href="index_transfer.php"><n>闲置转让</n></a>
						<img src="./img/贷款.png" style="left: 10px;top: 13px;">
					</li>
					<li class="head_sub">
						<a href="index_problem.php"><n>提问</n></a>
						<img src="./img/问题支持.png" style="left: 26px;top: 14px;">
					</li>
					<li class="head_sub">
						<a href="index_love.php"><n>表白</n></a>
						<img src="./img/收藏.png" style="width: 30px; left: 26px;top: 10px;">
					</li>
					<li class="head_sub">
						<a href="index_team.php"><n>组队</n></a>
						<img src="./img/用户群组.png" style="width: 27px;left: 26px;top: 11px;">
					</li>
					<li class="head_sub">
						<a href="index_debate.php"><n>吐槽</n></a>
						<img src="./img/问答.png" style="width: 23px;left: 26px;top: 14px;">
					</li>
				</ul>
			</div>
		</div>
		<div id="sort">
			<div class="container" style="padding-left: 20px;">
				<div style="float: left;">
					<span style="line-height: 50px;">排序方式：</span>
				</div>
				<ul>
					<li class="sort_sub">
						<button><n>上传时间</n></button>
					</li>
					<li class="sort_sub">
						<button><n>点赞数</n></button>
					</li>
				</ul>
			</div>
		</div>
		<!-- 投稿部分 -->
		<?php 
			include_once("CONN.php");
			$result=$connID->query('select * from `message`');
			$model=$result->fetch_all(MYSQLI_ASSOC);?>
			<!-- // $data=$model->getAll(); -->
			<!-- //for ($i=0; $i <= 5; $i++) { -->
				<!-- <div id="a"> -->
				<div id="main">
					<?php
					foreach($model as $myrow):
					if($myrow['area']!=null){?>
								<div class="main_massage" style="margin-left: 5.5px; margin-right: 0px;">
									<div class="massage_title">投稿作者:
										<?php echo $myrow['author'];?>
									</div>
									<div class="massage_content">投稿内容:
										<?php echo $myrow['matter'];?>
										<?php echo $myrow['pic'];?>
									</div>
									<div class="massage_uptime">上传时间:
										<?php echo $myrow['uptime'];?>
									</div>
									<div class="massage_operation">
										<ul>
											<li class="operation_sub">
												<a href=""><img src="./img/首页.png"></a>
												<n>点赞数量</n>
											</li>
											<li class="operation_sub">
												<a href=""><img src="./img/作品.png" ></a>
												<n>评论数量</n>
											</li>
											<li class="operation_sub">
												<n>举报数量</n>
												<a href=""><img src="./img/贷款.png"></a>
											</li>
										</ul>
									</div>
								</div>
						<!-- </div> -->
					<?php }?>
				<?php endforeach; ?>
				<!-- } -->
			</div> 
	</body>
</html>
